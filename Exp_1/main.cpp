#include <iostream>
#include <fstream>
#include <string>

using namespace std;
void checkNumber(string str){
    int num = 0; // for numeric value in the file
    int words = 0; // for words
    int letters = 0; // for alphabets

    int len = str.size();
    for(int i=0;i<len;i++){
        if(isdigit(str[i])){
            num++;
            continue;
        };
        if(str[i] == ' '){
            words++;
            continue;
        };
    }
    letters = len-words;
    cout << "words are " << words +1 << endl;
    cout << "letters are " << letters << endl;
    cout << "numbers are " << num << endl;

}
int main()
{
    /*============================================================================================================
    * write a programme to implemnt to count and show number of characters, alphbets, digits, and words in a file!
    ============================================================================================================*/

   string data;
   string output;
   // open a file in read mode.

   ifstream infile;
   infile.open("file.txt");

   if (!infile) {
       cout << "Unable to open file datafile.txt";
       exit(1);
   }

   while (std::getline(infile, data))output+=data;
   //cout << output << endl;
    checkNumber(output);

   // close the opened file.
   infile.close();

   return 0;
}
